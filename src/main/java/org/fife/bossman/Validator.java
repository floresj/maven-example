package org.fife.bossman;

import com.google.common.base.Strings;

public class Validator {

	public static boolean isNullOrEmpty(String value){
        // I used a static method in a class that uses Guava. Just demonstrating that
        // the Guava jar was included
        return Strings.isNullOrEmpty(value);		
	}
}
